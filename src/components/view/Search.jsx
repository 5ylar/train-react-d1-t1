import React from 'react'

function Search({handleOnchange,searchSubmit,search}){
        return (
            <div>
                <input
                    className="input-text"
                    type="text"
                    name="search"
                    onChange={handleOnchange}
                    value={search}
                />
                <button
                    className="btn-search"
                    onClick={searchSubmit}
                >search</button>
            </div>
        )
}


export default Search;



