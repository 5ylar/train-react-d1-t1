import React from 'react'

const ResultSearch = (props) => {
    return (
        <div>
            <div className="result-search">

                {
                    props.ResultSearch.map((value,index) => <li key={index}>{value}</li>)
                }
            </div>
        </div>
    )
}



export default ResultSearch;